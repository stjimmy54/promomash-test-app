using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Promomash.Features.Territories;
using Promomash.Features.Users;
using Promomash.Infrastructure;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Promomash.Features.Tests
{
    public class CreateUserCommandHandlerTests
    {
        private static PromomashDbContext CreateDbContext()
        {
            var options = new DbContextOptionsBuilder<PromomashDbContext>()
                .UseInMemoryDatabase("TestDatabase")
                .Options;
            return new PromomashDbContext(options);
        }
        private static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg => { });
            config.AssertConfigurationIsValid();
            return config.CreateMapper();
        }
        [Fact]
        public async Task ShouldCreateUser()
        {
            var context = CreateDbContext();

            var handler = new CreateUserCommandHandler(context, CreateMapper());
            var command = new CreateUserCommand()
            {
                Email = "AAA",
                Password = "BBB",
                Country = 1,
                Province = 3
            };
            var user = await handler.Handle(command, CancellationToken.None);
            
            Assert.True(user.Email == command.Email.ToLowerInvariant());
            Assert.True(user.CountryId == command.Country);
            Assert.True(user.ProvinceId == command.Province);
            Assert.True(user.PasswordHash != command.Password);
        }
    }
}
