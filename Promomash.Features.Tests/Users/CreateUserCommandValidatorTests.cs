﻿using Microsoft.EntityFrameworkCore;
using Promomash.Domain;
using Promomash.Features.Users;
using Promomash.Infrastructure;
using System.Threading.Tasks;
using Xunit;

namespace Promomash.Features.Tests.Users
{
    public class CreateUserCommandValidatorTests
    {
        private static PromomashDbContext CreateDbContext()
        {
            var options = new DbContextOptionsBuilder<PromomashDbContext>()
                .UseInMemoryDatabase("TestDatabase")
                .Options;
            return new PromomashDbContext(options);
        }
        [Fact]
        public async Task ShouldValidateEmptyEmail()
        {
            var context = CreateDbContext();

            var command = new CreateUserCommand()
            {
                Password = "BBB",
                Country = 1,
                Province = 3
            };
            var validator = new CreateUserCommandValidator(context);
            var result = await validator.ValidateAsync(command);
            Assert.Contains(result.Errors, x => x.PropertyName == nameof(command.Email));
        }
        [Fact]
        public async Task ShouldValidateEmailLike()
        {
            var context = CreateDbContext();

            var command = new CreateUserCommand()
            {
                Email = "test",
                Password = "BBB",
                Country = 1,
                Province = 3
            };
            var validator = new CreateUserCommandValidator(context);
            var result = await validator.ValidateAsync(command);
            Assert.Contains(result.Errors, x => x.PropertyName == nameof(command.Email));
        }
        [Fact]
        public async Task ShouldValidateDublicateEmails()
        {
            var context = CreateDbContext();
            await context.Users.AddAsync(new User()
            {
                Email = "mytestmail@test.com"
            });
            await context.SaveChangesAsync();

            var command = new CreateUserCommand()
            {
                Email = "mytestmail@test.com",
                Password = "BBB",
                Country = 1,
                Province = 3
            };
            var validator = new CreateUserCommandValidator(context);
            var result = await validator.ValidateAsync(command);
            Assert.Contains(result.Errors, x => x.PropertyName == nameof(command.Email));
        }
        [Fact]
        public async Task ShouldConfirmValidMail()
        {
            var context = CreateDbContext();
            var command = new CreateUserCommand()
            {
                Email = "mytestmail@test.com",
                Password = "BBB",
                Country = 1,
                Province = 3
            };
            var validator = new CreateUserCommandValidator(context);
            var result = await validator.ValidateAsync(command);
            Assert.DoesNotContain(result.Errors, x => x.PropertyName == nameof(command.Email));
        }
    }
}
