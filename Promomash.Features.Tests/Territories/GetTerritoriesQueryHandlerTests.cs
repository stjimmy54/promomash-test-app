using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Promomash.Domain;
using Promomash.Features.Territories;
using Promomash.Infrastructure;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Promomash.Features.Tests
{
    public class GetTerritoriesQueryHandlerTests
    {
        private static PromomashDbContext CreateDbContext()
        {
            var options = new DbContextOptionsBuilder<PromomashDbContext>()
                .UseInMemoryDatabase("TestDatabase")
                .Options;
            return new PromomashDbContext(options);
        }
        private static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg => cfg.AddProfile<TerritoryProfile>());
            config.AssertConfigurationIsValid();
            return config.CreateMapper();
        }
        [Fact]
        public async Task ShouldReturnRootsWhenParentIdIsEmpty()
        {
            var context = CreateDbContext();
            // Remove seed data
            context.Territories.RemoveRange(context.Territories);

            await context.Territories.AddRangeAsync(
                new Territory() { Id = 1, Name = "Root 1" },
                new Territory() { Id = 2, Name = "Root 2" }
                );
            await context.SaveChangesAsync();
            await context.Territories.AddRangeAsync(
                new Territory() { Id = 3, Name = "Child 1", ParentId = 1 },
                new Territory() { Id = 4, Name = "Child 1", ParentId = 2 }
            );
            await context.SaveChangesAsync();

            var handler = new GetTerritoriesQueryHandler(context, CreateMapper());
            var query = new GetTerritoriesQuery()
            {
                ParentId = null
            };
            var result = await handler.Handle(query, CancellationToken.None);
            Assert.True(result.FirstOrDefault(x => x.Id == 1) != null);
            Assert.True(result.FirstOrDefault(x => x.Id == 2) != null);
            Assert.True(result.FirstOrDefault(x => x.Id == 3) == null);
            Assert.True(result.FirstOrDefault(x => x.Id == 4) == null);
        }
        [Fact]
        public async Task ShouldReturnChildsWhenParentIdExists()
        {
            var context = CreateDbContext();
            // Remove seed data
            context.Territories.RemoveRange(context.Territories);

            await context.Territories.AddRangeAsync(
                new Territory() { Id = 1, Name = "Root 1" },
                new Territory() { Id = 2, Name = "Root 2" }
                );
            await context.SaveChangesAsync();
            await context.Territories.AddRangeAsync(
                new Territory() { Id = 3, Name = "Child 1", ParentId = 1 },
                new Territory() { Id = 4, Name = "Child 1", ParentId = 2 }
            );
            await context.SaveChangesAsync();

            var handler = new GetTerritoriesQueryHandler(context, CreateMapper());
            var query = new GetTerritoriesQuery()
            {
                ParentId = 1
            };
            var result = await handler.Handle(query, CancellationToken.None);
            Assert.True(result.FirstOrDefault(x => x.Id == 1) == null);
            Assert.True(result.FirstOrDefault(x => x.Id == 2) == null);
            Assert.True(result.FirstOrDefault(x => x.Id == 3) != null);
            Assert.True(result.FirstOrDefault(x => x.Id == 4) == null);
        }
    }
}
