﻿using MediatR;
using Promomash.Domain;

namespace Promomash.Features.Users
{
    public class CreateUserCommand : IRequest<User>
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public int Country { get; set; }
        public int Province { get; set; }
    }
}
