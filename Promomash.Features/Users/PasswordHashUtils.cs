﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System.Security.Cryptography;

namespace Promomash.Features.Users
{
    public static class PasswordHashUtils
    {
        public static byte[] CreateRandomSalt(int size = 128 / 8)
        {
            // generate a 128-bit(default) salt using a secure PRNG
            byte[] salt = new byte[size];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }
            return salt;
        }
        // derive a 256-bit(default) subkey (use HMACSHA1 with 10,000(default) iterations)
        public static byte[] CreateHash(string password, byte[] salt, int length = 256 / 8, int iterations = 10000)
        {
            var hashed = KeyDerivation.Pbkdf2(
            password: password,
            salt: salt,
            prf: KeyDerivationPrf.HMACSHA1,
            iterationCount: iterations,
            numBytesRequested: length);
            return hashed;
        }
    }
}
