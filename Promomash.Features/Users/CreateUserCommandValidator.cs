﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Promomash.Infrastructure;
using System.Linq;

namespace Promomash.Features.Users
{
    public class CreateUserCommandValidator : AbstractValidator<CreateUserCommand>
    {
        public CreateUserCommandValidator(PromomashDbContext db)
        {
            RuleFor(c => c.Email)
                .Cascade(CascadeMode.Stop)
                .NotEmpty()
                .EmailAddress()
                .MustAsync(async (email, token) =>
                {
                    var normalizedMail = email.ToLowerInvariant();
                    var exists = await db.Users.FirstOrDefaultAsync(x => x.Email == normalizedMail);
                    return exists == null;
                }).WithMessage("Email must be unique");

            RuleFor(c => c.Password)
                .Cascade(CascadeMode.Stop)
                .NotEmpty()
                .Matches("[A-Z]")
                .Matches("[0-9]");

            RuleFor(c => c.Country).Cascade(CascadeMode.Stop).NotEmpty().GreaterThan(0);
            RuleFor(c => c.Province).Cascade(CascadeMode.Stop).NotEmpty().GreaterThan(0);

        }
    }
}
