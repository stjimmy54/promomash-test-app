﻿using AutoMapper;
using MediatR;
using Promomash.Domain;
using Promomash.Infrastructure;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Promomash.Features.Users
{
    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, User>
    {
        private PromomashDbContext Db { get; set; }
        private IMapper Mapper { get; set; }
        public CreateUserCommandHandler(PromomashDbContext dbContext, IMapper mapper)
        {
            Db = dbContext;
            Mapper = mapper;
        }
        public async Task<User> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            var salt = PasswordHashUtils.CreateRandomSalt();
            var passwordHash = PasswordHashUtils.CreateHash(request.Password, salt);
            var user = new User()
            {
                Email = request.Email.ToLowerInvariant(),
                PasswordHash = Convert.ToBase64String(passwordHash),
                PasswordSalt = Convert.ToBase64String(salt),
                CountryId = request.Country,
                ProvinceId = request.Province
            };
            await Db.Users.AddAsync(user, cancellationToken);
            await Db.SaveChangesAsync();
            return user;
        }
    }
}
