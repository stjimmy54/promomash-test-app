﻿using MediatR;
using System.Collections.Generic;

namespace Promomash.Features.Territories
{
    public class GetTerritoriesQuery : IRequest<IEnumerable<TerritoryDto>>
    {
        public int? ParentId { get; set; }
    }
}
