﻿namespace Promomash.Features.Territories
{
    public class TerritoryDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? ParentId { get; set; }
    }
}
