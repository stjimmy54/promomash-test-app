﻿using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Promomash.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Promomash.Features.Territories
{
    public class GetTerritoriesQueryHandler : IRequestHandler<GetTerritoriesQuery, IEnumerable<TerritoryDto>>
    {
        private PromomashDbContext Db { get; set; }
        private IMapper Mapper { get; set; }
        public GetTerritoriesQueryHandler(PromomashDbContext dbContext, IMapper mapper)
        {
            Db = dbContext;
            Mapper = mapper;
        }
        public async Task<IEnumerable<TerritoryDto>> Handle(GetTerritoriesQuery request, CancellationToken cancellationToken)
        {
            var query = Db.Territories
                .AsNoTracking()
                .Where(x => x.DeletedAt == null || x.DeletedAt > DateTime.UtcNow);

            if (request.ParentId == null)
            {
                query = query.Where(x => x.ParentId == null);
            }
            else
            {
                query = query.Where(x => x.ParentId == request.ParentId);
            }
            return await Mapper.ProjectTo<TerritoryDto>(query).ToListAsync(cancellationToken);
        }
    }
}
