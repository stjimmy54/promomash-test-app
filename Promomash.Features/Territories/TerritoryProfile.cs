﻿using AutoMapper;
using Promomash.Domain;

namespace Promomash.Features.Territories
{
    public class TerritoryProfile : Profile
    {
        public TerritoryProfile()
        {
            CreateMap<Territory, TerritoryDto>();
        }
    }
}
