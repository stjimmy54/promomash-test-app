﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;

namespace Promomash.Features
{
    public static class Module
    {
        public static readonly Assembly Assembly = typeof(Module).Assembly;
        public static IServiceCollection AddFeaturesModule(this IServiceCollection services)
        {
            AssemblyScanner.FindValidatorsInAssembly(Assembly).ForEach(item => services.AddScoped(item.InterfaceType, item.ValidatorType));
            services.AddAutoMapper(Assembly);
            services.AddMediatR(new[] { Assembly });
            return services;
        }

    }
}
