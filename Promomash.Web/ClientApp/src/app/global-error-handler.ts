import { HttpErrorResponse } from "@angular/common/http";
import { Injectable, ErrorHandler, NgZone } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";

export interface ValidationError {
  ErrorCode: string;
  PropertyName: string;
  CurrentValue: string;
  Message: string;
}
export interface ValidationErrors extends Array<ValidationError> {}

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
  constructor(private snackbar: MatSnackBar, private zone: NgZone) {}

  handleError(error: Error) {
    let message = "";
    if (error instanceof HttpErrorResponse) {
      message = this.getHttpErrorResponseMessage(error);
    }
    message = message || error.message;
    this.zone.run(() => {
      this.snackbar.open(message, "Ok", {
        duration: 10 * 1000,
      });
    });

    console.error("Error from global error handler", error);
  }
  getHttpErrorResponseMessage(error: HttpErrorResponse): string {
    // handle validation errors
    if (error.status === 400) {
      const errors = (error.error as ValidationErrors) || [];
      return errors.map((e) => e.PropertyName + ": " + e.Message).join("\r\n");
    }
    return error.message;
  }
}
