import { HttpErrorResponse } from "@angular/common/http";
import { TestBed } from "@angular/core/testing";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { GlobalErrorHandler, ValidationErrors } from "./global-error-handler";

describe("GlobalErrorHandler", () => {
  let service: GlobalErrorHandler;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MatSnackBarModule],
      providers: [GlobalErrorHandler],
    });
  });

  it("should handle server validation errors", () => {
    service = TestBed.inject(GlobalErrorHandler);
    const fakeErrors: ValidationErrors = [
      {
        Message: "message",
        PropertyName: "property",
        CurrentValue: "value",
        ErrorCode: "code",
      },
    ];
    const fakeResponse = new HttpErrorResponse({
      error: fakeErrors,
      headers: undefined,
      status: 400,
      statusText: "",
      url: "",
    });
    expect(service.getHttpErrorResponseMessage(fakeResponse)).toBeTruthy();
  });
});
