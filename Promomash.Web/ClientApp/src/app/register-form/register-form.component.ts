import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
} from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  ValidatorFn,
  Validators,
} from "@angular/forms";
import { switchMap } from "rxjs/operators";
import { RegistrationService, TerritoryService } from "../api";
import { StepperSelectionEvent } from "@angular/cdk/stepper";
import { of } from "rxjs";

export const confirmPasswordValidator: ValidatorFn = (
  control: AbstractControl
) => {
  const password = control.get("password")?.value || "";
  const confirm = control.get("confirm")?.value || "";
  const result = password === confirm ? null : { confirm: true };
  control.get("confirm")?.setErrors(result);
  return result;
};

@Component({
  selector: "app-register-form",
  templateUrl: "register-form.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegisterFormComponent {
  userFormGroup = this.formBuilder.group(
    {
      username: [
        "",
        Validators.compose([Validators.required, Validators.email]),
      ],
      password: [
        "",
        Validators.compose([
          Validators.required,
          // Если нам нужна поддержка не только английского языка, то необходимо писать другой валидатор.
          // Для одного или двух языков можно использовать такой подход
          Validators.pattern(/[A-Z]/),
          Validators.pattern(/[\d]/),
        ]),
      ],
      confirm: ["", Validators.compose([Validators.required])],
      agree: ["", Validators.compose([Validators.requiredTrue])],
    },
    {
      validators: confirmPasswordValidator,
    }
  );
  addressFormGroup = this.formBuilder.group({
    country: ["", Validators.required],
    province: ["", Validators.required],
  });

  countries$ = this.territoryService.apiTerritoryGet({});
  provinces$ = this.addressFormGroup.get("country")!.valueChanges.pipe(
    switchMap((value) =>
      value != null
        ? this.territoryService.apiTerritoryGet({
            parentId: value.id,
          })
        : of([])
    )
  );
  isSuccessfull = false;

  constructor(
    private formBuilder: FormBuilder,
    private territoryService: TerritoryService,
    private registrationService: RegistrationService,
    private cdr: ChangeDetectorRef
  ) {}

  onStepChanged(event: StepperSelectionEvent) {
    if (
      event.selectedIndex == 2 &&
      this.addressFormGroup.valid &&
      this.userFormGroup.valid
    ) {
      this.submit();
    }
  }
  public submit() {
    this.registrationService
      .apiRegistrationPost({
        createUserCommand: {
          email: this.userFormGroup.value.username,
          password: this.userFormGroup.value.password,
          country: this.addressFormGroup.value.country.id,
          province: this.addressFormGroup.value.province.id,
        },
      })
      .subscribe(
        (v) => {
          console.log(v);
          this.isSuccessfull = true;
          this.cdr.markForCheck();
        },
        (e) => {
          this.isSuccessfull = false;
          this.cdr.markForCheck();
          throw e;
        }
      );
  }
}
