export * from './registration.service';
import { RegistrationService } from './registration.service';
export * from './territory.service';
import { TerritoryService } from './territory.service';
export const APIS = [RegistrationService, TerritoryService];
