﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Promomash.Features.Territories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Promomash.Web.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class TerritoryController : ControllerBase
    {
        private IMediator Mediator { get; set; }
        public TerritoryController(IMediator mediator)
        {
            Mediator = mediator;
        }

        [HttpGet]
        public async Task<IEnumerable<TerritoryDto>> Get([FromQuery] GetTerritoriesQuery query)
        {
            return await Mediator.Send(query);
        }
    }
}
