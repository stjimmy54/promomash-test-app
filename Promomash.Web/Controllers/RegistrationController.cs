﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Promomash.Features.Users;
using System.Threading.Tasks;

namespace Promomash.Web.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class RegistrationController : ControllerBase
    {
        private IMediator Mediator { get; set; }
        public RegistrationController(IMediator mediator)
        {
            Mediator = mediator;
        }

        [HttpPost]
        public async Task<long> Registration([FromBody] CreateUserCommand command)
        {
            var user = await Mediator.Send(command);
            return user.Id;
        }
    }
}
