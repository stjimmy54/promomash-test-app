﻿using FluentValidation;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;

namespace Promomash.Web.Infrastructure.Http
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;
        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context /* other dependencies */)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            HttpStatusCode httpCode;
            string result;


            var fluentValidationException = ex as ValidationException;

            if (fluentValidationException != null)
            {
                httpCode = HttpStatusCode.BadRequest;
                result = JsonSerializer.Serialize(fluentValidationException.Errors.Select(x => new { ErrorCode = x.ErrorCode, PropertyName = x.PropertyName, CurrentValue = x.AttemptedValue, Message = x.ErrorMessage }));
            }
            else
            {
                httpCode = HttpStatusCode.InternalServerError;
                var root = ex;
                if (root.InnerException != null)
                {
                    root = root.GetBaseException();
                    result = JsonSerializer.Serialize(new { error = ex.Message + "\r\n" + root.Message });
                }

                result = JsonSerializer.Serialize(new { error = ex.Message });
            }

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)httpCode;
            context.Response.Headers.Remove("Cache-Control");
            context.Response.Headers.Remove("Expires");
            context.Response.Headers.Add("Cache-Control", "no-cache, no-store");
            context.Response.Headers.Add("Expires", "-1");
            await context.Response.WriteAsync(result);
            await context.Response.CompleteAsync();
        }
    }
}
