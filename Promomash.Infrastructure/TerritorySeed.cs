﻿using Microsoft.EntityFrameworkCore;
using Promomash.Domain;
using System;

namespace Promomash.Infrastructure
{
    public static class TerritorySeed
    {
        public static ModelBuilder AddSeedTerritories(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Territory>().HasData(
                new Territory()
                {
                    Id = 1,
                    Name = "Роccийская федерация",
                    ParentId = null,
                    CreatedAt = DateTime.MinValue,
                    ModifiedAt = DateTime.MinValue
                },
                new Territory()
                {
                    Id = 2,
                    Name = "United States",
                    ParentId = null,
                    CreatedAt = DateTime.MinValue,
                    ModifiedAt = DateTime.MinValue
                },
                new Territory()
                {
                    Id = 3,
                    Name = "Москва",
                    ParentId = 1,
                    CreatedAt = DateTime.MinValue,
                    ModifiedAt = DateTime.MinValue
                },
                new Territory()
                {
                    Id = 4,
                    Name = "Московская область",
                    ParentId = 1,
                    CreatedAt = DateTime.MinValue,
                    ModifiedAt = DateTime.MinValue
                },
                new Territory()
                {
                    Id = 5,
                    Name = "Санкт-Петербург",
                    ParentId = 1,
                    CreatedAt = DateTime.MinValue,
                    ModifiedAt = DateTime.MinValue
                },
                new Territory()
                {
                    Id = 6,
                    Name = "Ленинградская область",
                    ParentId = 1,
                    CreatedAt = DateTime.MinValue,
                    ModifiedAt = DateTime.MinValue
                },
                new Territory()
                {
                    Id = 7,
                    Name = "Новосибирская область",
                    ParentId = 1,
                    CreatedAt = DateTime.MinValue,
                    ModifiedAt = DateTime.MinValue
                },
                new Territory()
                {
                    Id = 8,
                    Name = "Arizona",
                    ParentId = 2,
                    CreatedAt = DateTime.MinValue,
                    ModifiedAt = DateTime.MinValue
                },
                new Territory()
                {
                    Id = 9,
                    Name = "Califonia",
                    ParentId = 2,
                    CreatedAt = DateTime.MinValue,
                    ModifiedAt = DateTime.MinValue
                },
                new Territory()
                {
                    Id = 10,
                    Name = "Florida",
                    ParentId = 2,
                    CreatedAt = DateTime.MinValue,
                    ModifiedAt = DateTime.MinValue
                },
                new Territory()
                {
                    Id = 11,
                    Name = "New York",
                    ParentId = 2,
                    CreatedAt = DateTime.MinValue,
                    ModifiedAt = DateTime.MinValue
                },
                new Territory()
                {
                    Id = 12,
                    Name = "Texas",
                    ParentId = 2,
                    CreatedAt = DateTime.MinValue,
                    ModifiedAt = DateTime.MinValue
                }
             );
            return modelBuilder;
        }
    }
}
