﻿using Microsoft.EntityFrameworkCore;
using Promomash.Domain;

namespace Promomash.Infrastructure
{
    public class PromomashDbContext : DbContext
    {
        public PromomashDbContext(DbContextOptions<PromomashDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
        public DbSet<Territory> Territories { get; set; }
        public DbSet<User> Users { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(PromomashDbContext).Assembly);
            modelBuilder.AddSeedTerritories();
        }
    }
}
