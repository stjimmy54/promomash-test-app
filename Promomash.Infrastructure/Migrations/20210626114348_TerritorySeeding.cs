﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Promomash.Infrastructure.Migrations
{
    public partial class TerritorySeeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Territory",
                columns: new[] { "Id", "CreatedAt", "DeletedAt", "ModifiedAt", "Name", "ParentId" },
                values: new object[] { 1, new DateTime(2021, 6, 26, 11, 43, 47, 597, DateTimeKind.Utc).AddTicks(8996), null, new DateTime(2021, 6, 26, 11, 43, 47, 597, DateTimeKind.Utc).AddTicks(9003), "Роccийская федерация", null });

            migrationBuilder.InsertData(
                table: "Territory",
                columns: new[] { "Id", "CreatedAt", "DeletedAt", "ModifiedAt", "Name", "ParentId" },
                values: new object[] { 2, new DateTime(2021, 6, 26, 11, 43, 47, 597, DateTimeKind.Utc).AddTicks(9858), null, new DateTime(2021, 6, 26, 11, 43, 47, 597, DateTimeKind.Utc).AddTicks(9860), "United States", null });

            migrationBuilder.InsertData(
                table: "Territory",
                columns: new[] { "Id", "CreatedAt", "DeletedAt", "ModifiedAt", "Name", "ParentId" },
                values: new object[] { 3, new DateTime(2021, 6, 26, 11, 43, 47, 597, DateTimeKind.Utc).AddTicks(9862), null, new DateTime(2021, 6, 26, 11, 43, 47, 597, DateTimeKind.Utc).AddTicks(9862), "Москва", 1 });

            migrationBuilder.InsertData(
                table: "Territory",
                columns: new[] { "Id", "CreatedAt", "DeletedAt", "ModifiedAt", "Name", "ParentId" },
                values: new object[] { 4, new DateTime(2021, 6, 26, 11, 43, 47, 597, DateTimeKind.Utc).AddTicks(9864), null, new DateTime(2021, 6, 26, 11, 43, 47, 597, DateTimeKind.Utc).AddTicks(9864), "Московская область", 1 });

            migrationBuilder.InsertData(
                table: "Territory",
                columns: new[] { "Id", "CreatedAt", "DeletedAt", "ModifiedAt", "Name", "ParentId" },
                values: new object[] { 5, new DateTime(2021, 6, 26, 11, 43, 47, 597, DateTimeKind.Utc).AddTicks(9865), null, new DateTime(2021, 6, 26, 11, 43, 47, 597, DateTimeKind.Utc).AddTicks(9866), "Санкт-Петербург", 1 });

            migrationBuilder.InsertData(
                table: "Territory",
                columns: new[] { "Id", "CreatedAt", "DeletedAt", "ModifiedAt", "Name", "ParentId" },
                values: new object[] { 6, new DateTime(2021, 6, 26, 11, 43, 47, 597, DateTimeKind.Utc).AddTicks(9867), null, new DateTime(2021, 6, 26, 11, 43, 47, 597, DateTimeKind.Utc).AddTicks(9867), "Ленинградская область", 1 });

            migrationBuilder.InsertData(
                table: "Territory",
                columns: new[] { "Id", "CreatedAt", "DeletedAt", "ModifiedAt", "Name", "ParentId" },
                values: new object[] { 7, new DateTime(2021, 6, 26, 11, 43, 47, 597, DateTimeKind.Utc).AddTicks(9902), null, new DateTime(2021, 6, 26, 11, 43, 47, 597, DateTimeKind.Utc).AddTicks(9902), "Новосибирская область", 1 });

            migrationBuilder.InsertData(
                table: "Territory",
                columns: new[] { "Id", "CreatedAt", "DeletedAt", "ModifiedAt", "Name", "ParentId" },
                values: new object[] { 8, new DateTime(2021, 6, 26, 11, 43, 47, 597, DateTimeKind.Utc).AddTicks(9903), null, new DateTime(2021, 6, 26, 11, 43, 47, 597, DateTimeKind.Utc).AddTicks(9904), "Arizona", 2 });

            migrationBuilder.InsertData(
                table: "Territory",
                columns: new[] { "Id", "CreatedAt", "DeletedAt", "ModifiedAt", "Name", "ParentId" },
                values: new object[] { 9, new DateTime(2021, 6, 26, 11, 43, 47, 597, DateTimeKind.Utc).AddTicks(9905), null, new DateTime(2021, 6, 26, 11, 43, 47, 597, DateTimeKind.Utc).AddTicks(9906), "Califonia", 2 });

            migrationBuilder.InsertData(
                table: "Territory",
                columns: new[] { "Id", "CreatedAt", "DeletedAt", "ModifiedAt", "Name", "ParentId" },
                values: new object[] { 10, new DateTime(2021, 6, 26, 11, 43, 47, 597, DateTimeKind.Utc).AddTicks(9907), null, new DateTime(2021, 6, 26, 11, 43, 47, 597, DateTimeKind.Utc).AddTicks(9907), "Florida", 2 });

            migrationBuilder.InsertData(
                table: "Territory",
                columns: new[] { "Id", "CreatedAt", "DeletedAt", "ModifiedAt", "Name", "ParentId" },
                values: new object[] { 11, new DateTime(2021, 6, 26, 11, 43, 47, 597, DateTimeKind.Utc).AddTicks(9909), null, new DateTime(2021, 6, 26, 11, 43, 47, 597, DateTimeKind.Utc).AddTicks(9909), "New York", 2 });

            migrationBuilder.InsertData(
                table: "Territory",
                columns: new[] { "Id", "CreatedAt", "DeletedAt", "ModifiedAt", "Name", "ParentId" },
                values: new object[] { 12, new DateTime(2021, 6, 26, 11, 43, 47, 597, DateTimeKind.Utc).AddTicks(9910), null, new DateTime(2021, 6, 26, 11, 43, 47, 597, DateTimeKind.Utc).AddTicks(9911), "Texas", 2 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Territory",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Territory",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Territory",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Territory",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Territory",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Territory",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Territory",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Territory",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Territory",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Territory",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Territory",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Territory",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
