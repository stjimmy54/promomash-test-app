﻿namespace Promomash.Domain
{
    public class User
    {
        public long Id { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }
        public int CountryId { get; set; }
        public virtual Territory Country { get; set; }
        public int ProvinceId { get; set; }
        public virtual Territory Province { get; set; }

    }
}
